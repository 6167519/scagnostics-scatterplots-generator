from scagnosticscnn.use_cases import scatterplot_use_cases as uc
from scagnosticscnn.domain.generator import datagenerator as dg
from scagnosticscnn.domain.qualitymetrics.qualitymetrics import QualityMetricsFactory
from scagnosticscnn.domain.qualitymetrics.scagnosticservice import ScagnosticsServiceBuilder

from scagnosticscnn.use_cases import request_objects as req
from scagnosticscnn.shared import response_object as resp

generateCount = 3


# 'Outlying':0.04400014091235556
# 'Skewed':0.6069003636047836
# 'Clumpy':0.0
# 'Sparse':0.1112798743879008
# 'Striated':0.08333333333333333
# 'Convex':0.5207301147643973
# 'Skinny':0.6845955768639076
# 'Stringy':0.41057538546581634
# 'Monotonic':0.012075190291199208

def test_scatterplot_request_with_qualitymetrics_serial():
    factory = QualityMetricsFactory()
    factory.register_builder(
        'SCAGNOSTICS', ScagnosticsServiceBuilder())

    config = {
        'scagnostics_bins': 50,
        'scagnostics_remove_outliers': True,
    }

    scagnosticsService = factory.create('SCAGNOSTICS', **config)

    generatorRandom = dg.RandomDataGenerator()
    scatterplot_with_qm_request_use_case = uc.ScatterplotWithQM_Serial_RequestUseCase(
        generatorRandom, scagnosticsService)

    count = generateCount
    request_object = req.ScatterplotWithQMRequestObject.from_dict(
        {'filters': {
            'count': count,
            'numPoints': 100,
            'domainMin': 0,
            'domainMax': 10000,
            'numClasses': 5,
            'qmfilters': [{'Clumpy': [0.0, 0.2]}]
        }}
    )
    filterObj = request_object.filters
    assert filterObj['domainMin'] == 0
    filterqmlist = filterObj['qmfilters']
    predicate = filterqmlist[0]
    for key, value in predicate.items():
        assert key == 'Clumpy'
        assert value == [0.0, 0.2]
    assert request_object.filters['domainMin'] == 0

    response_object = scatterplot_with_qm_request_use_case.execute(request_object)

    assert bool(response_object) is True
    assert response_object.type == resp.ResponseSuccess.SUCCESS
    assert len(response_object.value) == count

    for sp in response_object.value:
        scagTest = scagnosticsService.calculateQM(sp)
        assert scagTest['Clumpy'] >= 0.0 and scagTest['Clumpy'] <= 0.2


def test_scatterplot_request_with_qualitymetrics_parallel():
    factory = QualityMetricsFactory()
    factory.register_builder(
        'SCAGNOSTICS', ScagnosticsServiceBuilder())

    config = {
        'scagnostics_bins': 50,
        'scagnostics_remove_outliers': True,
    }

    scagnosticsService = factory.create('SCAGNOSTICS', **config)

    generator = dg.RandomDataGenerator()
    scatterplot_with_qm_request_use_case = uc.ScatterplotWithQM_Parallel_RequestUseCase(
        generator, scagnosticsService)

    count = generateCount
    request_object = req.ScatterplotWithQMRequestObject.from_dict(
        {'filters': {
            'count': count,
            'numPoints': 100,
            'domainMin': 0,
            'domainMax': 10000,
            'numClasses': 5,
            'qmfilters': [{'Clumpy': [0.0, 0.2]}]
        }}
    )
    filterObj = request_object.filters
    assert filterObj['domainMin'] == 0
    filterqmlist = filterObj['qmfilters']
    predicate = filterqmlist[0]
    for key, value in predicate.items():
        assert key == 'Clumpy'
        assert value == [0.0, 0.2]
    assert request_object.filters['domainMin'] == 0

    response_object = scatterplot_with_qm_request_use_case.execute(request_object)

    assert bool(response_object) is True
    assert response_object.type == resp.ResponseSuccess.SUCCESS
    assert len(response_object.value) == count

    for sp in response_object.value:
        scagTest = scagnosticsService.calculateQM(sp)
        assert scagTest['Clumpy'] >= 0.0 and scagTest['Clumpy'] <= 0.2


def test_scatterplot_request_with_qualitymetrics_permutation():
    factory = QualityMetricsFactory()
    factory.register_builder(
        'SCAGNOSTICS', ScagnosticsServiceBuilder())

    config = {
        'scagnostics_bins': 50,
        'scagnostics_remove_outliers': True,
    }

    scagnosticsService = factory.create('SCAGNOSTICS', **config)

    generator = dg.RandomDataGenerator()
    scatterplot_with_qm_request_use_case = uc.ScatterplotWithQM_Permutation_RequestUseCase(
        generator, scagnosticsService)

    count = generateCount
    request_object = req.ScatterplotWithQMRequestObject.from_dict(
        {'filters': {
            'count': count,
            'numPoints': 100,
            'domainMin': 0,
            'domainMax': 10000,
            'numClasses': 5,
            'qmfilters': [{'Clumpy': [0.0, 0.2]}]
        }}
    )
    filterObj = request_object.filters
    assert filterObj['domainMin'] == 0
    filterqmlist = filterObj['qmfilters']
    predicate = filterqmlist[0]
    for key, value in predicate.items():
        assert key == 'Clumpy'
        assert value == [0.0, 0.2]
    assert request_object.filters['domainMin'] == 0

    response_object = scatterplot_with_qm_request_use_case.execute(request_object)

    assert bool(response_object) is True
    assert response_object.type == resp.ResponseSuccess.SUCCESS
    assert len(response_object.value) == count

    for sp in response_object.value:
        scagTest = scagnosticsService.calculateQM(sp)
        assert scagTest['Clumpy'] >= 0.0 and scagTest['Clumpy'] <= 0.2


def test_scatterplot_request_with_qualitymetrics_permutation_multiple():
    factory = QualityMetricsFactory()
    factory.register_builder(
        'SCAGNOSTICS', ScagnosticsServiceBuilder())

    config = {
        'scagnostics_bins': 50,
        'scagnostics_remove_outliers': True,
    }

    scagnosticsService = factory.create('SCAGNOSTICS', **config)

    generator = dg.RandomDataGenerator()
    scatterplot_with_qm_request_use_case = uc.ScatterplotWithQM_Permutation_RequestUseCase(
        generator, scagnosticsService)

    count = 5
    request_object = req.ScatterplotWithQMRequestObject.from_dict(
        {'filters': {
            'count': count,
            'numPoints': 100,
            'domainMin': 0,
            'domainMax': 10000,
            'numClasses': 5,
            'qmfilters': [{'Clumpy': [0.0, 0.2]}, {'Convex': [0.55, 0.60]}]
        }}
    )
    filterObj = request_object.filters
    assert filterObj['domainMin'] == 0
    filterqmlist = filterObj['qmfilters']
    predicate = filterqmlist[0]
    for key, value in predicate.items():
        assert key == 'Clumpy'
        assert value == [0.0, 0.2]
    assert request_object.filters['domainMin'] == 0

    response_object = scatterplot_with_qm_request_use_case.execute(request_object)

    assert bool(response_object) is True
    assert response_object.type == resp.ResponseSuccess.SUCCESS
    assert len(response_object.value) == count

    for sp in response_object.value:
        scagTest = scagnosticsService.calculateQM(sp)
        assert scagTest['Clumpy'] >= 0.0
        assert scagTest['Clumpy'] <= 0.2
        assert scagTest['Convex'] >= 0.55
        assert scagTest['Convex'] <= 0.60


def test_scatterplot_request_with_getQueueObjects():
    factory = QualityMetricsFactory()
    factory.register_builder(
        'SCAGNOSTICS', ScagnosticsServiceBuilder())

    config = {
        'scagnostics_bins': 50,
        'scagnostics_remove_outliers': True,
    }

    scagnosticsService = factory.create('SCAGNOSTICS', **config)

    generator = dg.RandomDataGenerator()
    scatterplot_with_qm_request_use_case = uc.ScatterplotWithQM_Parallel_RequestUseCase(
        generator, scagnosticsService, recordIntermediateSteps=True)

    count = 5
    request_object = req.ScatterplotWithQMRequestObject.from_dict(
        {'filters': {
            'count': count,
            'numPoints': 100,
            'domainMin': 0,
            'domainMax': 10000,
            'numClasses': 5,
            'qmfilters': [{'Clumpy': [0.0, 0.2]}]
        }}
    )
    response_object = scatterplot_with_qm_request_use_case.execute(request_object)
    assert bool(response_object) is True

    assert bool(scatterplot_with_qm_request_use_case.getFromQueue()) is True
