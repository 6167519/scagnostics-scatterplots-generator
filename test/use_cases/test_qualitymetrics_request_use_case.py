from scagnosticscnn.use_cases import qualitymetrics_use_cases as uc
from scagnosticscnn.domain.generator import datagenerator as dg
from scagnosticscnn.use_cases import request_objects as req

from scagnosticscnn.domain.qualitymetrics.qualitymetrics import QualityMetricsFactory
from scagnosticscnn.domain.qualitymetrics.scagnosticservice import ScagnosticsServiceBuilder


def test_scagnostics_request_without_parameters():
    factory = QualityMetricsFactory()
    factory.register_builder(
        'SCAGNOSTICS', ScagnosticsServiceBuilder())

    qm_request_use_case = uc.QualityMetricsRequestUseCase(factory)

    generator = dg.RandomDataGenerator()
    scatterplot = generator.scatterplot()[0]

    request_object = req.QualityMetricsRequestObject.from_dict(
        {'scatterplot': scatterplot,
         'qualitymetric': 'SCAGNOSTICS'})
    response_object = qm_request_use_case.execute(request_object)

    assert bool(response_object) is True
    assert response_object.value['Skinny']
    assert len(response_object.value.keys()) == 11


def test_scagnostics_request_with_parameters():
    factory = QualityMetricsFactory()
    factory.register_builder(
        'SCAGNOSTICS', ScagnosticsServiceBuilder())

    scagnostics_request_use_case = uc.QualityMetricsRequestUseCase(factory)

    generator = dg.RandomDataGenerator()
    scatterplot = generator.scatterplot()[0]

    config = {
        'scagnostics_bins': 10,
        'scagnostics_remove_outliers': True
    }
    request_object = req.QualityMetricsRequestObject.from_dict(
        {'scatterplot': scatterplot,
         'qualitymetric': 'SCAGNOSTICS',
         'config': config})
    response_object = scagnostics_request_use_case.execute(request_object)

    assert bool(response_object) is True
    assert response_object.value['Skinny']
    assert len(response_object.value.keys()) == 11
