import pytest


from scagnosticscnn.app import create_app
from scagnosticscnn.settings import TestConfig


@pytest.yield_fixture(scope='function')
def app():
    return create_app(TestConfig)
