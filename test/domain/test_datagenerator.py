import uuid
from scagnosticscnn.domain.generator import datagenerator as dg
from scagnosticscnn.domain.scatterplot import ScatterPlot


def test_initialize():
    generator = dg.RandomDataGenerator()
    assert generator


def test_initialize_with_seed():
    generator = dg.RandomDataGenerator(101)
    assert generator

    generator = dg.RandomDataGenerator(None)
    assert generator

    generator = dg.RandomDataGenerator("Seed")
    assert generator


def test_make_scatterplot():
    generator = dg.RandomDataGenerator(101)

    scatterPlotData = generator.scatterplot()
    assert scatterPlotData
    scatterPlot = ScatterPlot.from_dict(scatterPlotData[0])
    assert scatterPlot


def test_make_several_scatterplots():
    generator = dg.RandomDataGenerator()

    count = 10
    scatterPlotData = generator.scatterplot({'count': count})
    assert scatterPlotData

    assert len(scatterPlotData) == count

    for spData in scatterPlotData:
        scatterPlot = ScatterPlot.from_dict(spData)
        assert scatterPlot


def test_with_filters_scatterplots():
    generator = dg.RandomDataGenerator()

    count = 10
    filter = {
        'count': count,
        'domainMin': 100,
        'domainMax': 200,
        'numPoints': 10000,
        'numClasses': 2
    }
    scatterPlotData = generator.scatterplot(filter)
    assert scatterPlotData

    assert len(scatterPlotData) == count

    for spData in scatterPlotData:
        scatterPlot = ScatterPlot.from_dict(spData)
        assert scatterPlot
        assert scatterPlot.domainMin == filter.get('domainMin')
        assert scatterPlot.domainMax == filter.get('domainMax')
        assert scatterPlot.numPoints == filter.get('numPoints')
        assert scatterPlot.numClasses == filter.get('numClasses')
