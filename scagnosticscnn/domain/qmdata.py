from collections.abc import MutableMapping

from scagnosticscnn.shared.domain_model import DomainModel
import uuid
from datetime import datetime
import numpy as np

import json

import copy


class QualityMetricsData(MutableMapping):

    def __init__(self, id, typeString, qmdata, *args, **kwargs):
        if not id:
            id = uuid.uuid4()
        self.id = id

        if not qmdata:
            raise ValueError("QMData should not be empty")
        # self.qmdata = qmdata

        if not type:
            raise ValueError("type needs to explain which qm is calculated/stored here")
        self.type = typeString.lower()

        self.store = dict()
        self.update(dict(qmdata, **kwargs))  # use the free update to set keys

    @classmethod
    def from_dict(cls, adict):
        myid = adict['id']
        mytype = adict['type']

        qmdata = copy.deepcopy(adict)
        qmdata.pop('id', None)
        qmdata.pop('type', None)

        qmDataReturn = QualityMetricsData(
            id=myid,
            typeString=mytype,
            qmdata=adict
        )

        return qmDataReturn

    def to_dict(self):
        meta = {
            'id': self.id,
            'type': self.type
        }
        merge = {**meta, **self.store}

        return merge

    def __eq__(self, other):
        return self.to_dict() == other.to_dict()

    def __hash__(self):
        return hash(tuple((k, v) for k, v in self.to_dict().items()))

    def __getitem__(self, key):
        return self.store[self.__keytransform__(key)]

    def __setitem__(self, key, value):
        self.store[self.__keytransform__(key)] = value

    def __delitem__(self, key):
        del self.store[self.__keytransform__(key)]

    def __iter__(self):
        return iter(self.store)

    def __len__(self):
        return len(self.store)

    def __keytransform__(self, key):
        return key

    def jsonDump(self, fp=None, *args, **kwargs):
        mydict = self.to_dict()

        if fp is None:
            return json.dumps(mydict, *args, **kwargs)
        else:
            kwargs.setdefault("indent", 4)

            try:
                fp.write
                return json.dump(mydict, fp, *args, **kwargs)
            except AttributeError:
                with open(fp, "w") as f:
                    f.write(json.dumps(mydict, *args, **kwargs))


DomainModel.register(QualityMetricsData)
