from scagnosticscnn.shared.domain_model import DomainModel
import uuid
from datetime import datetime
import numpy as np


class ScatterPlot(object):

    def __init__(self, id, domainMin, domainMax, points, classes, **kwargs):
        if not id:
            id = uuid.uuid4()
        self.id = id

        self.domainMin = domainMin
        self.domainMax = domainMax

        if points is None:
            raise ValueError('Points array must be present for a ScatterPlot object.')
        self.points = points
        self.numPoints = len(points)

        self.classes = classes
        self.numClasses = len(set(classes))

        self.timestamp = kwargs.get('timestamp', datetime.now())
        self.datasetName = kwargs.get('datasetName', "Default ScatterPlot")

        if domainMax <= domainMin:
            raise ValueError("Value Domain invalid and max <= min")

        if self.numClasses > self.numPoints:
            raise ValueError("Class assignment invalid: More classes than points")

        for p in self.points:
            if p[0] < domainMin or p[0] > domainMax:
                raise ValueError("Point below or above domainValue: point: " +
                                 str(p[0]) + " min/max: " + str(domainMin) + "/" + str(domainMax))
            if p[1] < domainMin or p[1] > domainMax:
                raise ValueError("Point below or above domainValue: point: " +
                                 str(p[1]) + " min/max: " + str(domainMin) + "/" + str(domainMax))

    @classmethod
    def from_dict(cls, adict):
        sp = ScatterPlot(
            id=adict['id'],
            domainMin=adict['domainMin'],
            domainMax=adict['domainMax'],
            points=adict['points'],
            classes=adict['classes'],
            datasetName=adict['datasetName'],
            timestamp=adict['timestamp']
        )

        return sp

    def to_dict(self):
        return {
            'id': self.id,
            'domainMin': self.domainMin,
            'domainMax': self.domainMax,
            'points': self.points,
            'numPoints': self.numPoints,
            'classes': self.classes,
            'numClasses': self.numClasses,
            'timestamp': self.timestamp,
            'datasetName': self.datasetName
        }

    def __eq__(self, other):
        return self.to_dict() == other.to_dict()

    def __hash__(self):
        return hash(tuple((k, v) for k, v in self.to_dict().items()))


DomainModel.register(ScatterPlot)
