from scagnosticscnn.shared import use_case as uc
from scagnosticscnn.domain.scatterplot import ScatterPlot

from multiprocessing import Process, Queue


class QualityMetricsRequestUseCase(uc.UseCase):
    """
    Base class for all QM Request Use Cases.
    Provides get_value to get a scatterplot and _isInFilterRange for checking if scatterplot is in qm range
    """

    def __init__(self, generator, qm, recordIntermediateSteps=False, queueMaxSize=200):
        self.generator = generator
        self.qm = qm
        self.recordIntermediateSteps = recordIntermediateSteps
        self.queue = Queue(queueMaxSize)

    def _isInFilterRange(self, qmresult, qmFilters):
        # 'qmfilters': [{'Clumpy': [0.0, 0.2]}]
        for filter in qmFilters:
            for qmName, qmRange in filter.items():
                if qmresult[qmName] < qmRange[0] or qmresult[qmName] > qmRange[1]:
                    return False

        return True

    def putInQueue(self, scatterplot, qmresult):
        if self.recordIntermediateSteps == False:
            return

        from datetime import datetime
        obj = {
            'scatterplot': scatterplot,
            'qmresult': qmresult,
            'timestamp': datetime.now()
        }
        self.queue.put(obj)

    def getFromQueue(self):
        if self.queue.empty():
            return {}
        return self.queue.get()
