import matplotlib.pyplot as plt
import numpy as np
np.random.seed(19680801)


fig, ax = plt.subplots()
for color in ['tab:blue', 'tab:orange', 'tab:green']:
    n = 750
    x, y = np.random.rand(2, n)
    scale = 200.0 * np.random.rand(n)
    ax.scatter(x, y, c=color, s=scale, label=color,
               alpha=0.3, edgecolors='none')

ax.legend()
ax.grid(True)

plt.show()

# plt.savefig('foo.png')

# savefig('foo.png', bbox_inches='tight')


# import numpy as np
# import matplotlib.pyplot as plt

# # Create data
# N = 60
# g1 = (0.6 + 0.6 * np.random.rand(N), np.random.rand(N))
# g2 = (0.4+0.3 * np.random.rand(N), 0.5*np.random.rand(N))
# g3 = (0.3*np.random.rand(N), 0.3*np.random.rand(N))

# data = (g1, g2, g3)
# colors = ("red", "green", "blue")
# groups = ("coffee", "tea", "water")

# # Create plot
# fig = plt.figure()
# ax = fig.add_subplot(1, 1, 1, axisbg="1.0")

# for data, color, group in zip(data, colors, groups):
#     x, y = data
#     ax.scatter(x, y, alpha=0.8, c=color, edgecolors='none', s=30, label=group)

# plt.title('Matplot scatter plot')
# plt.legend(loc=2)
# plt.show()

# import numpy as np
# import matplotlib.pyplot as plt

# # Fixing random state for reproducibility
# np.random.seed(19680801)


# N = 50
# x = np.random.rand(N)
# y = np.random.rand(N)
# colors = np.random.rand(N)
# area = (30 * np.random.rand(N))**2  # 0 to 15 point radii

# plt.scatter(x, y, s=area, c=colors, alpha=0.5)
# plt.show()


# import numpy as np
# import matplotlib.pyplot as plt

# # Create data
# N = 500
# x = np.random.rand(N)
# y = np.random.rand(N)
# colors = (0, 0, 0)
# area = np.pi*3

# # Plot
# plt.scatter(x, y, s=area, c=colors, alpha=0.5)
# plt.title('Scatter plot pythonspot.com')
# plt.xlabel('x')
# plt.ylabel('y')
# plt.show()
