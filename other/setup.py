#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup

# with open('README.rst') as readme_file:
#     readme = readme_file.read()

# with open('HISTORY.rst') as history_file:
#     history = history_file.read()

requirements = [
    # TODO: put package requirements here
]

test_requirements = [
    # TODO: put package test requirements here

]

setup(
    name='scagnosticscnn',
    version='0.1.0',
    description="A clean architecture project in Python for developing data and training a Scagnostics CNN",
    long_description=readme + '\n\n' + history,
    author="Michael Behrisch",
    author_email='m.behrisch@uu.nl',
    url='https://git.science.uu.nl/m.behrisch/scagnostics-cnn',
    packages=[
        'scagnsticscnn',
    ],
    package_dir={'scagnsticscnn':
                 'scagnsticscnn'},
    include_package_data=True,
    install_requires=requirements,
    license="MIT license",
    zip_safe=False,
    keywords='scagnostics, CNN, visual quality metrics, information visualization',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Researchers',
        'License :: OSI Approved :: MIT License',
        'Natural Language :: English',
        "Programming Language :: Python :: 2",
        'Programming Language :: Python :: 2.6',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
    ],
    test_suite='test',
    tests_require=test_requirements
)
